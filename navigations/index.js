import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import Login from "../src/screens/Login";
import Sigup from "../src/screens/Sigup";
import Intro from "../src/screens/Intro";
import Rules from "../src/screens/Rules";
import Forgot from "../src/screens/Forgot";
import BottomNavigator from "./BottomNavigator";
import Demo from "../src/screens/demo";
import Weather from "../src/screens/Weather";
import Device from "../src/screens/Device";
import Notifi from "../src/screens/Notifi";
const Stack = createStackNavigator();

const RootNavigation = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Intro">
        {/* <Stack.Screen name="Intro" component={Intro} options={{
                    headerShown: false
                }} /> */}
        <Stack.Screen
          name="BottomNavigator"
          component={BottomNavigator}
          options={{
            headerShown: false,
          }}
        />
        {/* <Stack.Screen name="Sigup" component={Sigup} />
        <Stack.Screen name="Rules" component={Rules} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Forgot" component={Forgot} /> */}
        <Stack.Screen
          name="Device"
          component={Device}
          options={{ headerShown: false }}
        />
        {/* <Stack.Screen name="Notifi" component={Notifi} /> */}
        {/* <Stack.Screen name="Demo" component={Demo} />  */}
        {/* <Stack.Screen name="Weather" component={Weather} /> */}
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default RootNavigation;
