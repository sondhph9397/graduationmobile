import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import {
  Ionicons,
  FontAwesome,
  FontAwesome5,
  Feather,
  MaterialIcons,
} from "@expo/vector-icons";
import { TouchableOpacity } from "react-native-gesture-handler";
import { useNavigation } from "@react-navigation/native";

import Home from "../src/screens/Home";
import Sigup from "../src/screens/Sigup";
import Rules from "../src/screens/Rules";
import Forgot from "../src/screens/Forgot";
import Device from "../src/screens/Device";
import Notifi from "../src/screens/Notifi";
const Tab = createBottomTabNavigator();
const BottomTabNavigator = () => {
  const navigation = useNavigation();
  const onPressDevice = () => navigation.navigate("Device");

  return (
    <Tab.Navigator
      tabBarOptions={{
        style: {
          justifyContent: "center",
          paddingVertical: 10,
          backgroundColor: "#eff4f0",
          elevation: 5,
          shadowColor: "#000",
          shadowOffset: {
            width: 40,
            height: 30,
          },
        },
        activeTintColor: "tomato",
        inactiveTintColor: "gray",
      }}
    >
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: "",
          tabBarIcon: ({ color, size }) => (
            <TouchableOpacity>
              <Ionicons name="home-outline" size={24} color="black" />
            </TouchableOpacity>
          ),
        }}
      />

      <Tab.Screen
        name="Sigup"
        component={Sigup}
        options={{
          tabBarLabel: "",
          tabBarIcon: ({ color, size }) => (
            <TouchableOpacity>
              <FontAwesome name="bookmark-o" size={24} color="black" />
            </TouchableOpacity>
          ),
        }}
      />
      <Tab.Screen
        name="Device"
        component={Device}
        options={{
          tabBarLabel: "",
          tabBarIcon: ({ color, size }) => (
            <TouchableOpacity
              style={{
                borderRadius: 50,
                backgroundColor: "#3699FF",
                padding: 5,
                bottom: "50%",
              }}
            >
              <MaterialIcons name="device-hub" size={40} color="white" />
            </TouchableOpacity>
          ),
        }}
      />
      <Tab.Screen
        name="Notifi"
        component={Notifi}
        options={{
          tabBarLabel: "",
          tabBarIcon: ({ color, size }) => (
            <TouchableOpacity>
              <Feather name="bell" size={24} color="black" />
            </TouchableOpacity>
          ),
        }}
      />
      <Tab.Screen
        name="Forgot"
        component={Forgot}
        options={{
          tabBarLabel: "",
          tabBarIcon: ({ color, size }) => (
            <TouchableOpacity>
              <FontAwesome5 name="user" size={24} color="black" />
            </TouchableOpacity>
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const Stack = createStackNavigator();
const screenOptionStyle = {
  headerShown: false,
};

const HomeStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Login" component={BottomTabNavigator} />
      <Stack.Screen name="Sigup" component={Sigup} />
    </Stack.Navigator>
  );
};

export default HomeStackNavigator;
