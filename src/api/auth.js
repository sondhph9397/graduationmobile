import request from "./request";

export const login = (data) => {
    return request.post('api/login', data);
}

export const logout = () => {
    return request.post('api/logout');
}
export const profile = () => {
    return request.post('api/profile');
}

export const device = () => {
    return request.get('api/data-log');
}

export const status = () => {
    return request.post('api/save-record?device_id=1&temperature=25.3&humidity=50.3&temperatureWater=26&co2=451&ph=8');
}
