import React, { useState, useEffect, Component } from 'react';
import { View, Text, StyleSheet, Button, Image, ImageBackground, Keyboard, } from 'react-native'
import { TextInput, TouchableOpacity, TouchableWithoutFeedback } from 'react-native-gesture-handler'
import RNPickerSelect from 'react-native-picker-select';
import * as ImagePicker from 'expo-image-picker';
import { Checkbox } from 'react-native-paper';
import { useNavigation } from "@react-navigation/native";

import Avatar from '../images/avata-defaul.png'

const Sigup = () => {
    const navigation = useNavigation()
    const onPressRules = () => navigation.navigate('Rules');


    const [image, setImage] = useState(null);
    useEffect(() => {
        (async () => {
            if (Platform.OS !== 'web') {
                const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
                if (status !== 'granted') {
                    alert('Sorry, we need camera roll permissions to make this work!');
                }
            }
        })();
    }, []);

    const pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });

        console.log(result);

        if (!result.cancelled) {
            setImage(result.uri);
        }
    };

    const placeholder = {
        label: 'Gender',
        value: null,
        color: '#9EA0A4',
    };

    const [checked, setChecked] = React.useState(false);

    return (
        <View style={styles.container}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View style={styles.avatar}>
                    <TouchableOpacity style={{ borderRadius: 100 }} onPress={pickImage}>
                        <ImageBackground source={Avatar} style={{ width: 100, height: 100, borderRadius: 100 }}>
                            {image && <Image source={{ uri: image }} style={{ width: 100, height: 100, borderRadius: 100 }} />}
                        </ImageBackground>
                    </TouchableOpacity>
                </View>
                <View style={styles.form}>
                    <TextInput style={styles.formSigup} placeholder="Name"></TextInput>
                    <TextInput style={styles.formSigup} placeholder="Name@gmail.com"></TextInput>
                    <TextInput style={styles.formSigup} keyboardType='numeric' placeholder="Phone"></TextInput>
                    <TextInput style={styles.formSigup} placeholder="Password"></TextInput>
                    <TextInput style={styles.formSigup} placeholder="Address"></TextInput>
                    <View style={styles.formSigup}>
                        <RNPickerSelect placeholder={placeholder}
                            onValueChange={(value) => console.log(value)}
                            items={[
                                { label: 'Man', value: '0' },
                                { label: 'Woman', value: '1' },
                                { label: 'Other', value: '2' },
                            ]}
                        />
                    </View>
                    <View style={styles.select}>
                        <Checkbox
                            status={checked ? 'checked' : 'unchecked'}
                            onPress={() => {
                                setChecked(!checked);
                            }}
                        />
                        <Text style={styles.textSelect}>Tôi đồng ý với mọi <Text onPress={onPressRules} style={{ color: '#3699FF', textDecorationLine: 'underline' }}>điều khoản và điều kiện</Text></Text>
                    </View>
                    <View style={styles.button}>
                        <TouchableOpacity style={styles.buttonSigup}>
                            <Text style={styles.text}>Đăng ký</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </View >
    )
}

export default Sigup;

const styles = StyleSheet.create({
    container: {
        justifyContent: 'flex-start',
        flex: 1,
        backgroundColor: '#fff',
    },
    avatar: {
        alignItems: 'center',
        paddingVertical: 30
    },
    form: {
        paddingHorizontal: 30
    },
    formSigup: {
        borderColor: '#707070',
        borderWidth: 1,
        marginBottom: 10,
        paddingLeft: 20,
        borderRadius: 10,
        height: 50
    },
    select: {
        flexDirection: 'row'
    },
    textSelect: {
        marginTop: 6,
        color: '#707070'
    },
    button: {
        marginVertical: 30
    },
    buttonSigup: {
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#3699FF',
        borderWidth: 1,
        borderRadius: 100,
    },
    text: {
        color: '#3699FF',
        fontWeight: 'bold'
    }
})