import React, { useEffect, useState } from "react";
import { status } from "../api/auth";
import { View, Text } from "react-native";

const Demo = () => {
  const [devices, setDevices] = useState([]);

  useEffect(() => {
    const getDeviceApi = async () => {
      const res = await status();
      setDevices(res.data);
      console.log(res);
    };

    getDeviceApi();
  }, []);

  return (
    <View>
      <Text></Text>
    </View>
  );
};
export default Demo;
