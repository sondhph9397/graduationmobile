import React, { useState } from 'react'
import { View, Text, StyleSheet, Image, Keyboard } from 'react-native';
import { TouchableNativeFeedback, TouchableOpacity } from 'react-native-gesture-handler';
import { TextInput } from 'react-native-paper';
import { useNavigation } from "@react-navigation/native";

import fogot from '../images/fogot.png'

const Forgot = ({ }) => {
    const navigation = useNavigation()
    const onPressLogin = () => navigation.navigate('Login');

    return (
        <View style={styles.container}>
            <TouchableNativeFeedback onPress={Keyboard.dismiss}>
                <View style={styles.image}>
                    <Image source={fogot} style={styles.logo} />
                </View>
                <View style={styles.form}>
                    <Text style={styles.title}>Quên mật khẩu</Text>
                    <Text style={styles.context}>Nhập email đăng ký để lấy lại mật khẩu</Text>
                    <TextInput style={styles.input} placeholder='email@gmail.com'></TextInput>
                    <TouchableOpacity style={styles.button}><Text style={styles.text}>Hoàn tất</Text></TouchableOpacity>
                    <View style={styles.send}>
                        <Text>Bạn đã có tài khoản?<Text style={{ color: '#3699FF', paddingLeft: 4, }} onPress={onPressLogin}>Đăng nhập</Text></Text>
                    </View>
                </View>
            </TouchableNativeFeedback>
        </View>

    )
}

export default Forgot;

const styles = StyleSheet.create({
    container: {
        justifyContent: 'flex-start',
        flex: 1,
        backgroundColor: '#fff',
    },
    image: {
        width: '100%',
        alignItems: 'center',
        marginBottom: 30
    },
    logo: {
        width: '90%',
        resizeMode: 'contain',
        height: 300,
    },
    form: {
        paddingHorizontal: 30
    },
    title: {
        fontWeight: 'bold',
        fontSize: 30,
        color: '#3699FF',
    },
    context: {
        fontSize: 14,
        color: '#707070',
        marginBottom: 20
    },
    input: {
        borderColor: '#707070',
        borderWidth: 1,
        marginBottom: 10,
        paddingLeft: 10,
        borderRadius: 10,
        height: 50,
        backgroundColor: '#fff'
    },
    button: {
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#3699FF',
        borderRadius: 100,
        marginTop: 30,
        marginBottom: 20,
    },
    text: {
        fontWeight: 'bold',
        color: 'white'
    },
    send: {
        alignItems: 'center'
    }
})