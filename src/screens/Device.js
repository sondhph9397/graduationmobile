import React, { useEffect, useState } from "react";
import { device } from "../api/auth";
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Image,
  Switch,
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";

import BG from "../images/device-bg.png";
import BG1 from "../images/icon-bg.png";
import BG2 from "../images/icon1-bg.png";
import ic1 from "../images/temp.png";
import ic2 from "../images/hump.png";
import ic3 from "../images/artboard.png";
import ic4 from "../images/ph.png";
import ic5 from "../images/co2.png";
import ic6 from "../images/o2.png";
import light from "../images/light.png";
import pump from "../images/pump.png";
import suct from "../images/suct.png";
const Device = () => {
  const [devices, setDevices] = useState([]);

  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);

  useEffect(() => {
    const getDeviceApi = async () => {
      const res = await device();
      setDevices(res.data);
    };

    getDeviceApi();
  }, []);

  return (
    <View style={styles.container}>
      <ImageBackground source={BG} style={styles.background}>
        <ScrollView>
          <View style={styles.flex}>
            <View style={styles.rowLeft}>
              <View style={styles.left}>
                <View style={styles.smallIcon}>
                  <Image source={ic1}></Image>
                  <View style={styles.small}>
                    <Text style={styles.title}>Nhiệt độ</Text>
                    <Text style={styles.spec}>
                      {devices.temperature + " °C"}
                    </Text>
                  </View>
                </View>
              </View>
              <View style={styles.left}>
                <View style={styles.smallIcon}>
                  <Image source={ic2}></Image>
                  <View style={styles.small}>
                    <Text style={styles.title}>Độ ẩm</Text>
                    <Text style={styles.spec}> {devices.humidity + " % "}</Text>
                  </View>
                </View>
              </View>
              <View style={styles.left}>
                <View style={styles.smallIcon}>
                  <Image source={ic3}></Image>
                  <View style={styles.small}>
                    <Text style={styles.title}>Dưới nước</Text>
                    <Text style={styles.spec}>
                      {devices.temperatureWater + " °C "}
                    </Text>
                  </View>
                </View>
              </View>
              <View style={styles.left}>
                <View style={styles.smallIcon}>
                  <Image source={ic4}></Image>
                  <View style={styles.small}>
                    <Text style={styles.title}>Độ pH</Text>
                    <Text style={styles.spec}>{devices.ph + " H+ "}</Text>
                  </View>
                </View>
              </View>
              <View style={styles.left}>
                <View style={styles.smallIcon}>
                  <Image source={ic5}></Image>
                  <View style={styles.small}>
                    <Text style={styles.title}>Lượng Co2</Text>
                    <Text style={styles.spec}>{devices.co2 + " g/l "}</Text>
                  </View>
                </View>
              </View>
              <View style={styles.left}>
                <View style={styles.smallIcon}>
                  <Image source={ic6}></Image>
                  <View style={styles.small}>
                    <Text style={styles.title}>Lượng O2</Text>
                    <Text style={styles.spec}>{devices.o2 + " m/l "}</Text>
                  </View>
                </View>
              </View>
            </View>
            <View style={styles.rowRight}>
              <View style={styles.right}>
                <View style={styles.smallDevice}>
                  <Image source={light} style={styles.image}></Image>
                  <View style={styles.textDevice}>
                    <Text style={styles.nameDevice}>Trạng thái đèn</Text>
                    <Text style={styles.status}>a</Text>
                    <Switch
                      trackColor={{ false: "#767577", true: "#81b0ff" }}
                      thumbColor={isEnabled ? "#3699FF" : "#f4f3f4"}
                      ios_backgroundColor="#3e3e3e"
                      onValueChange={toggleSwitch}
                      value={isEnabled}
                    />
                  </View>
                </View>
              </View>
              <View style={styles.right}>
                <View style={styles.smallDevice}>
                  <Image source={pump} style={styles.image}></Image>
                  <View style={styles.textDevice}>
                    <Text style={styles.nameDevice}>Trạng thái bơm</Text>
                    <Text style={styles.status}>a</Text>
                    <Switch
                      trackColor={{ false: "#767577", true: "#81b0ff" }}
                      thumbColor={isEnabled ? "#3699FF" : "#f4f3f4"}
                      ios_backgroundColor="#3e3e3e"
                      onValueChange={toggleSwitch}
                      value={isEnabled}
                    />
                  </View>
                </View>
              </View>
              <View style={styles.right}>
                <View style={styles.smallDevice}>
                  <Image source={suct} style={styles.image}></Image>
                  <View style={styles.textDevice}>
                    <Text style={styles.nameDevice}>Trạng thái sục</Text>
                    <Text style={styles.status}>a</Text>
                    <Switch
                      trackColor={{ false: "#767577", true: "#81b0ff" }}
                      thumbColor={isEnabled ? "#3699FF" : "#f4f3f4"}
                      ios_backgroundColor="#3e3e3e"
                      onValueChange={toggleSwitch}
                      value={isEnabled}
                    />
                  </View>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.btnOn}>
            <Text style={styles.textOn}>Tự động bật</Text>
            <View>
              <Switch
                trackColor={{ false: "#767577", true: "#81b0ff" }}
                thumbColor={isEnabled ? "#3699FF" : "#f4f3f4"}
                ios_backgroundColor="#3e3e3e"
                onValueChange={toggleSwitch}
                value={isEnabled}
              />
            </View>
          </View>
        </ScrollView>
      </ImageBackground>
    </View>
  );
};
export default Device;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  background: {
    flex: 1,
    paddingHorizontal: 10,
    paddingTop: 50,
  },
  flex: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  rowLeft: {
    flexDirection: "column",
  },
  left: {
    borderRadius: 15,
    marginVertical: 10,
    height: 100,
    backgroundColor: "#e6e1de",
    elevation: 5,
    padding: 5,
  },
  rowRight: {
    flexDirection: "column",
  },
  right: {
    width: 200,
    height: 200,
    backgroundColor: "#e6e1de",
    marginVertical: 10,
    elevation: 10,
    borderRadius: 15,
    padding: 10,
  },
  smallIcon: {
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  title: {
    fontSize: 14,
    fontWeight: "bold",
    color: "#009182",
  },
  spec: {
    fontSize: 20,
    color: "#2D8800",
    fontWeight: "bold",
    textAlign: "center",
  },
  small: {
    paddingTop: 10,
  },
  btnOn: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: 10,
  },
  textOn: {
    fontSize: 16,
    color: "#3699FF",
    fontWeight: "bold",
  },
  smallDevice: {
    flexDirection: "row",
  },
  image: {
    resizeMode: "contain",
    width: "50%",
    height: 150,
  },
  textDevice: {
    flexDirection: "column",
    justifyContent: "space-around",
    marginTop: 70,
  },
  nameDevice: {
    fontWeight: "bold",
    color: "#3699FF",
  },
  status: {
    color: "#2D8800",
    fontWeight: "bold",
    textAlign: "right",
  },
});
