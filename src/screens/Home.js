import React, { useState, useEffect } from "react";
import { View, Text, Image, ImageBackground, StyleSheet } from "react-native";
import { TextInput, ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { DrawerActions } from '@react-navigation/native';
import { NavigationContainer, StackActions, useNavigation } from "@react-navigation/native";

import axios from 'axios'
import { device } from '../api/auth'

import BG from '../images/Background-home.png'
import avata from '../images/avata-defaul.png'

const Home = ({ navigation }) => {
    const [data, setData] = useState({})
    useEffect(() => {
        const fecthData = async () => {
            const res = await axios.get('https://api.openweathermap.org/data/2.5/weather?q=Hanoi&appid=e1143868484898a1d4f93f5de8b52d65')
            setData(res.data)
            console.log(res)
        }
        fecthData();
    }, [])

    const [devices, setDevices] = useState([])

    useEffect(() => {
        const getDeviceApi = async () => {
            const res = await device();
            setDevices(res.data)
            console.log(res)
        }

        getDeviceApi();
    }, [])


    const navigations = useNavigation()
    const onPressWeather = () => navigations.navigate('Weather');
    return (
        <View style={styles.container}>
            <View style={styles.up}>
                <ImageBackground source={BG} style={styles.background}>
                    <View style={styles.header}>
                        <TouchableOpacity style={styles.avata}>
                            <Image source={avata} style={styles.imgAvata} />
                            <Text>Hi,everyone</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.scroll}>
                        <ScrollView scrollEventThrottle={16} vertical>
                            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                                <TouchableOpacity style={styles.scrollUp} onPress={onPressWeather}>
                                    {/* <Text>{(data.main.temp - 273.15).toFixed(2) + " °C"}</Text> */}
                                    <Text>{data.name}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.scrollUp}>

                                </TouchableOpacity>
                                <TouchableOpacity style={styles.scrollUp}>

                                </TouchableOpacity>
                            </ScrollView>
                        </ScrollView>
                    </View>
                </ImageBackground>
            </View>
            <View style={styles.down}>
                <View style={styles.tutorial}><Text style={styles.title}>Hướng dẫn sử dụng</Text>
                    <Text style={styles.context}>Chúng tôi rất vui mừng chào đón bạn đã tin dùng và sử dụng sản phẩm của chúng tôi.
                    Sản phẩm của chúng tôi giúp người nông dân có thể áp dụng công nghệ cao vào nuôi tôm một cách thông minh.
                    có thể sử dụng sản phẩm, vui lòng đọc kỹ hướng dẫn về cách thức hoạt động của sản phẩm...</Text>
                    <TouchableOpacity><Text style={styles.more}>Đọc thêm</Text></TouchableOpacity>
                </View>
                <View style={styles.recommended}>
                    <Text style={styles.texRecommend}>Đề nghị</Text>
                    <TouchableOpacity style={styles.buttonMore}><Text style={styles.textMore}>Đọc thêm</Text></TouchableOpacity>
                </View>
                <View>
                    <ScrollView scrollEventThrottle={16} vertical>
                        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                            <TouchableOpacity style={styles.scrollDown}>

                            </TouchableOpacity>
                            <TouchableOpacity style={styles.scrollDown}>

                            </TouchableOpacity>
                            <TouchableOpacity style={styles.scrollDown}>

                            </TouchableOpacity>
                        </ScrollView>
                    </ScrollView>
                </View>
            </View>
        </View>
    )
};
export default Home;

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1,
    },
    header: {
        flexDirection: 'row',
        paddingHorizontal: 10,
        paddingTop: 50,
        justifyContent: 'space-between'
    },
    avata: {
        flexDirection: 'row',

    },
    imgAvata: {
        height: 50,
        width: 50,
        marginRight: 10
    },
    up: {
        height: '25%',
    },
    background: {
        flex: 1,

    },
    down: {
        paddingTop: '25%',
        paddingHorizontal: 10
    },
    scroll: {
        position: 'absolute',
        top: '50%',
        paddingHorizontal: 10

    },
    scrollUp: {
        height: 150,
        marginRight: 10,
        marginTop: 20,
        borderRadius: 20,
        marginBottom: 10,
        width: 160,
        backgroundColor: '#fff',
        elevation: 5,
    },
    tutorial: {
        backgroundColor: '#3699FF',
        borderRadius: 10,
        paddingHorizontal: 10,
        paddingVertical: 10,
    },
    title: {
        fontWeight: 'bold',
        color: '#fff',
        marginBottom: 5
    },
    context: {
        fontSize: 12,
        color: '#fff',
    },
    more: {
        color: '#0D00E6',
        textDecorationLine: 'underline',
        fontWeight: 'bold'
    },
    recommended: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 15
    },
    texRecommend: {
        color: '#707070',
        fontWeight: 'bold',
        fontSize: 16,

    },
    buttonMore: {
        borderRadius: 5,
        backgroundColor: '#3699FF',
    },
    textMore: {
        color: '#fff',
        padding:3
    },
    scrollDown: {
        height: 200,
        elevation: 5,
        marginRight: 10,
        borderRadius: 20,
        marginBottom: 10,
        width: 160,
        backgroundColor: 'pink'
    }
})