import React, { useState } from 'react'
import { View, Text, Image, Alert, StyleSheet, TouchableOpacity, TouchableWithoutFeedback, Keyboard } from 'react-native'
import { ScrollView, TextInput, } from 'react-native-gesture-handler'
import { login } from '../api/auth'
import { NavigationContainer, StackActions, useNavigation } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import Logo from '../images/logo.png'


const Stack = createStackNavigator();

const Login = ({ }) => {
    const navigation = useNavigation()
    const onPressSigup = () => navigation.navigate('Sigup');
    const onPressForgot = () => navigation.navigate('Forgot');

    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [errors, setErrors] = useState();
    const handleLogin = (data) => {
        setErrors({});
        login({ email, password })
            .then((response) => {
                console.log(response.data);
                Alert.alert("success");
                navigation.navigate("BottomNavigator");
            })
            .catch(({ response }) => {
                console.log(response.data);
                if (response.data.errors) {
                    setErrors(response.data.errors);
                    return;
                }
                Alert.alert("false");
            });
    }

    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={styles.container}>
                <View style={styles.image}>
                    <Image style={styles.logo} source={Logo} />
                </View>
                <View style={styles.formLogin}>
                    <Text style={styles.title}>Đăng nhập</Text>
                    <View>
                        <TextInput style={styles.form} placeholder='Email' value={email} onChangeText={(e) => setEmail(e)}></TextInput>
                        {/* <Text style={styles.errors}>{errors?.email?.[0]}</Text> */}
                        <TextInput style={styles.form} placeholder='Password' secureTextEntry={true} value={password} onChangeText={(e) => setPassword(e)}></TextInput>
                        {/* <Text style={styles.errors}>{errors?.password?.[0]}</Text> */}
                        <TouchableOpacity onPress={onPressForgot}><Text style={styles.forget}>Quên mật khẩu?</Text></TouchableOpacity>
                    </View>

                    <TouchableOpacity style={styles.button}>
                        <Text style={styles.text} onPress={handleLogin}>Đăng nhập</Text>
                    </TouchableOpacity>
                    <View style={styles.sigup}>
                        <Text>Bạn chưa có tài khoản?<Text style={{ color: '#3699FF', paddingLeft: 4, }} onPress={onPressSigup}> Đăng ký</Text></Text>
                    </View>

                </View>
            </View>
        </TouchableWithoutFeedback>
    )
}
export default Login;

const styles = StyleSheet.create({
    container: {
        justifyContent: 'flex-start',
        flex: 1,
        backgroundColor: '#fff',
    },
    image: {
        width: '100%',
        alignItems: 'center',

    },
    logo: {
        width: '70%',
        resizeMode: 'contain',
        height: 280,

    },
    formLogin: {
        paddingHorizontal: 40
    },
    title: {
        fontSize: 30,
        fontWeight: 'bold',
        color: '#3699FF',
        paddingBottom: 20
    },
    form: {
        borderColor: '#707070',
        borderWidth: 1,
        marginBottom: 10,
        paddingLeft: 20,
        borderRadius: 10,
        height: 50
    },
    forget: {
        color: '#3699FF',
        fontSize: 15,
        textAlign: 'right'
    },
    button: {
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#3699FF',
        borderRadius: 100,
        marginTop: 30,
        marginBottom: 20,
    },
    text: {
        fontWeight: 'bold',
        color: 'white'
    },
    sigup: {
        alignItems: 'center'
    },
    errors: {
        color: 'red'
    }
})