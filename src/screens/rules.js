import React, { useState } from 'react'
import { StyleSheet } from 'react-native';
import { View, Text } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

const Rules = ({ }) => {
    return (
        <View style={styles.container}>
                <ScrollView>
            <Text style={styles.content}>
                Nội dung của các điều khoản này
            Dù biết rằng bạn rất dễ bỏ qua những Điều khoản dịch vụ này, nhưng chúng tôi cần phải nêu rõ trách nhiệm của chúng tôi cũng như trách nhiệm của bạn trong quá trình bạn sử dụng các dịch vụ của Google.
            Các Điều khoản dịch vụ này phản ánh cách thức kinh doanh của Google, những điều luật mà công ty chúng tôi phải tuân theo và một số điều mà chúng tôi vẫn luôn tin là đúng. Do đó, các Điều khoản dịch vụ này giúp xác định mối quan hệ giữa Google với bạn khi bạn tương tác với các dịch vụ của chúng tôi. Ví dụ: Các điều khoản này trình bày các chủ đề sau:

            Trách nhiệm của chúng tôi: Đây là phần mô tả cách chúng tôi cung cấp và phát triển các dịch vụ của mình
            Trách nhiệm của bạn: Phần này nêu ra một số quy tắc mà bạn phải tuân theo khi sử dụng các dịch vụ của chúng tôi
            Nội dung trong các dịch vụ của Google: Phần này mô tả quyền sở hữu trí tuệ đối với nội dung mà bạn thấy trong các dịch vụ của chúng tôi, bất kể nội dung đó thuộc về bạn, Google hay người khác
            Trong trường hợp xảy ra vấn đề hoặc bất đồng: Phần này mô tả các quyền hợp pháp khác mà bạn có và những điều bạn nên biết trong trường hợp có người vi phạm các điều khoản này
            Việc hiểu rõ các điều khoản này là rất quan trọng vì bằng việc sử dụng các dịch vụ của chúng tôi, bạn đồng ý với các điều khoản này.

            Bên cạnh các điều khoản này, chúng tôi cũng ban hành Chính sách quyền riêng tư. Mặc dù không nằm trong các điều khoản này, nhưng đây là một chính sách bạn nên đọc để hiểu rõ hơn cách bạn có thể cập nhật, quản lý, xuất và xóa thông tin của mình.

            Nhà cung cấp dịch vụ
            Pháp nhân sau đây cung cấp các dịch vụ của Google và ký kết hợp đồng với bạn:

            Google LLC
            được thành lập theo luật của tiểu bang Delaware, Hoa Kỳ và hoạt động theo luật pháp Hoa Kỳ

            1600 Amphitheatre Parkway
            Mountain View, California 94043
            Hoa Kỳ

            Yêu cầu về độ tuổi
            Nếu chưa đủ tuổi để tự quản lý Tài khoản Google theo quy định, bạn phải được cha mẹ hoặc người giám hộ hợp pháp cho phép thì mới có thể sử dụng Tài khoản Google. Vui lòng yêu cầu cha mẹ hoặc người giám hộ hợp pháp cùng bạn đọc các điều khoản này.

            Nếu bạn là cha mẹ hoặc người giám hộ hợp pháp và bạn cho phép con bạn sử dụng các dịch vụ này, thì bạn phải tuân theo các điều khoản này và chịu trách nhiệm đối với hoạt động của con bạn trong các dịch vụ đó.

Một số dịch vụ của Google có các yêu cầu bổ sung về độ tuổi như mô tả trong các chính sách và điều khoản bổ sung dành riêng cho từng dịch vụ.
            </Text>
                </ScrollView>
        </View>
    )
}
export default Rules;

const styles = StyleSheet.create ({
    container:{
        flex:1,
        backgroundColor:'#fff',
        
        paddingHorizontal:10,
    },
    content:{
        lineHeight:20,
        fontSize:16,
        paddingVertical:20
    }
})