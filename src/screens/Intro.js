import React from 'react'
import { Text, View, Image, StyleSheet } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Swiper from 'react-native-swiper'
import { useNavigation } from "@react-navigation/native";

import BG1 from '../images/Background_1.png'
import BG2 from '../images/Background_2.png'
import BG3 from '../images/Background_3.png'
import BG4 from '../images/Background_4.png'



export default () => {
    const navigation = useNavigation()
    const onPressLogin = () => navigation.navigate('Login');
    const onPressSigup = () => navigation.navigate('Sigup');

    return (
        <Swiper style={styles.wrapper} loop={false} activeDot={<View style={{ width: 20, height: 8, borderRadius: 5, backgroundColor: '#3699FF' }}></View>}>
            <View style={styles.slide1}>
                <Image source={BG1} style={styles.image} />
                <Text style={styles.title}>Kết Nối Thông Minh</Text>
                <Text style={styles.content}>Kết nối và điều khiển thiết bị của bạn một cách dễ dàng và thông minh</Text>
            </View>

            <View style={styles.slide2}>
                <Image source={BG2} style={styles.image} />
                <Text style={styles.title}>Tích Hợp Đa Nền Tảng</Text>
                <Text style={styles.content}>Ứng dụng có thể được sử dụng trên các nền tảng và hệ
điều hành khác nhau</Text>
            </View>

            <View style={styles.slide3}>
                <Image source={BG3} style={styles.image} />
                <Text style={styles.title}>An Toàn Khi Sử Dụng</Text>
                <Text style={styles.content}>Bảo mật thông tin của quý khách hàng và truy cập
    an toàn khi sử dụng</Text>
            </View>

            <View style={styles.slide4}>
                <Image source={BG4} style={styles.image} />
                <Text style={styles.title}>Trải Nghiệm Ngay Bây Giờ</Text>
                <Text style={styles.content}>Cảm ơn bạn đã sử dụng dịch vụ của chúng tôi để
    tiếp tục truy cập vui lòng đăng nhập để tiếp tục</Text>
                <View style={styles.button}>
                    <TouchableOpacity onPress={onPressSigup}>
                        <View style={styles.buttonLogin}>
                            <Text style={styles.textLogin}>Đăng ký</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={onPressLogin}>
                        <View style={styles.buttonLogin}>
                            <Text style={styles.textLogin}>Đăng nhập</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </Swiper>
    )
}

const styles = StyleSheet.create({
    wrapper: {},
    slide1: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: 'white',
        paddingHorizontal: 10,
    },
    slide2: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: 'white',
        paddingHorizontal: 10,
    },
    slide3: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: 'white',
        paddingHorizontal: 10,
    },
    slide4: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: 'white',
        paddingHorizontal: 10,
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold'
    },
    image: {
        width: '100%',
        resizeMode: 'contain',
        height: 464

    },
    title: {
        fontSize: 25,
        fontWeight: 'bold',
        color: '#3699FF',
        paddingVertical: 10
    },
    content: {
        color: '#707070',
        paddingHorizontal: 10,
        fontSize: 15,
        textAlign: 'center'
    },
    button: {
        flexDirection: 'row',
        padding: 20
    },
    buttonLogin: {
        borderWidth: 1,
        borderColor: '#3699FF',
        height: 50,
        width: 100,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 100,
        marginRight: 20,
    },
    textLogin: {
        fontWeight: 'bold',
        color: '#3699FF'
    }
})

