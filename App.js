import React from 'react';

import RootNavigation from "./navigations";

const App = () => <RootNavigation />

export default App;
